import React, { Component} from "react";
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from "react-native";

export default class App extends Component{

  constructor (props){
    super(props);
    this.state = {
      textValue: '',
      valor2: '',
    };
  }

  changeTextInput = text =>{
    this.setState({textValue: text});
  };

  segundoValor = password => {
    this.setState({valor2: password});
  };

  render(){
    return (
      <View style={styles.container}>
        
        <View style={styles.image}>
        <Image source={require('./img/logo.png')}/> 
        </View>
        <View style={styles.text}>
          <Text style={{color: 'red',fontSize: 26}}>Iniciar Sesión</Text>
        </View>
        <View style={styles.box2}>
        <Text style={{paddingBottom: 5, color: 'blue',fontSize:20}}>Usuario</Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1,}}
          onChangeText = {text => this.changeTextInput(text)}
          value={this.state.textValue}/></View>
        <View style={styles.box2}>
        <Text style={{paddingBottom: 5, color:'blue',fontSize:20}}>Contraseña</Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1,}}
          onChangeText = {password => this.segundoValor(password)}
          value={this.state.valor2}
        /></View>

        <TouchableOpacity style={styles.button}>
          <Text style={{fontSize:19}}> Ingresar </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 45,
  },

  text: {
    alignItems: 'center',
    padding: 10,
  },

  image: {
    alignItems: 'center',
    padding: 30,
  },

  button: {
    top: 25,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 15,
  },

  box2:{
    padding: 15,
  },

});